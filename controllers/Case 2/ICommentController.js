import IComment from "../../models/Case 2/ICommentModel.js"

export const getTotalIComment = async (req, res) => {
    try {
        const iComments = await IComment.find()
        const totalComment = TotalComment(iComments, 0)
        let data = {
            totalComment: totalComment
        }
        res.status(200).json(data)
    } catch (e) {
        console.log(e.message)
    }
}

export const createIComment = async (req, res) => {
    try {
        await new IComment({
            commentId: req.body.commentId,
            commentContent: req.body.commentContent,
            replies: req.body.replies
        }).save()
        res.status(201).json({ msg: "IComment Created" })
    } catch (e) {
        console.log(e.message)
    }
}

const TotalComment = (iComments, i) => {
    if (iComments === undefined) return i
    iComments.forEach((iComment) => {
        i = TotalComment(iComment.replies, i+1)
    })
    return i
}