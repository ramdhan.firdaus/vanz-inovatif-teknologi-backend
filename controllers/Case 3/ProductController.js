import Product from '../../models/Case 3/ProductModel.js'

export const getProduct = async (req, res) => {
    try {
        const product = await Product.find()
        let data = {
            product: product
        }
        res.status(200).json(data)
        
    } catch (e) {
        console.log(e.message)
    }
}

export const addProduct = async (req, res) => {
    try {
        const product = await Product.find()
        const id = product.length + 1 // Asumsi tidak ada yang dihapus
        await new Product({
            productId: id,
            nameProduct: req.body.nameProduct,
            store: req.body.store
        }).save()
        res.status(201).json({ msg: "Product Created" })
    } catch (e) {
        console.log(e.message)
    }
}