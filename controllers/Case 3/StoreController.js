import Store from '../../models/Case 3/StoreModel.js'

export const getStore = async (req, res) => {
    try {
        const store = await Store.find()
        let data = {
            store: store
        }
        res.status(200).json(data)
    } catch (e) {
        console.log(e.message)
    }
}

export const addStore = async (req, res) => {
    try {
        const store = await Store.find()
        const id = store.length + 1 // Asumsi tidak ada yang dihapus
        await new Store({
            storeId: id,
            nameStore: req.body.nameStore,
        }).save()
        res.status(201).json({ msg: "Store Created" })
    } catch (e) {
        console.log(e.message)
    }
}