import Fruit from "../../models/Case 1/FruitModel.js"

// Number 1
export const getFruitByName = async(req, res) => {
    try {
        const fruits = await Fruit.find()
        let dataName = []
        fruits.forEach((fruit) => {
            dataName.push(fruit.fruitName)
        })
        console.log("Buah yang dimiliki Andi:")
        console.log(dataName)
        res.status(200).json(dataName)
    } catch (e) {
        console.log(e.message)
    }
}

// Number 2
export const getFruitBasket = async (req, res) => {
    try {
        const fruits = await Fruit.find()
        let dataFruit = {}
        let array = []
        fruits.forEach((fruit) => {
            if (dataFruit[fruit.fruitType] == undefined) {
                dataFruit[fruit.fruitType] = []
            }
            array = dataFruit[fruit.fruitType]
            array.push(fruit.fruitName)
            dataFruit[fruit.fruitType] = array
        })
        
        let totalBasket = Object.keys(dataFruit).length
        console.log(`Jumlah Wadah Yang Dibutuhkan adalah ${totalBasket}`)

        for (let [key, value] of Object.entries(dataFruit)) {
            console.log(`Buah pada wadah ${key} yaitu ${value}`)
        }

        let data = {
            totalBasket: totalBasket,
            fruitInBasket: dataFruit
        }
        
        res.status(200).json(data)
    } catch (e) {
        console.log(e.message)
    }
}

// Number 3
export const getCountFruitBasket = async (req, res) => {
    try {
        const fruits = await Fruit.find()
        let dataFruit = {}
        fruits.forEach((fruit) => {
            if (dataFruit[fruit.fruitType] == undefined) {
                dataFruit[fruit.fruitType] = 0
            }
            dataFruit[fruit.fruitType] = dataFruit[fruit.fruitType] + fruit.stock
        })

        for (let [key, value] of Object.entries(dataFruit)) {
            console.log(`Stok buah pada wadah ${key} berjumlah ${value}`)
        }

        res.status(200).json(dataFruit)
    } catch (e) {
        console.log(e.message)
    }
}

// Number 2 and 3
export const getFruitBasketAndCount = async (req, res) => {
    try {
        const fruits = await Fruit.find()
        let dataFruit = {}
        let array = []
        fruits.forEach((fruit) => {
            if (dataFruit[fruit.fruitType] == undefined) {
                dataFruit[fruit.fruitType] = {}
                dataFruit[fruit.fruitType].fruit = []
                dataFruit[fruit.fruitType].totalFruit = 0
            }
            array = dataFruit[fruit.fruitType].fruit
            array.push(fruit.fruitName)
            dataFruit[fruit.fruitType].fruit = array

            dataFruit[fruit.fruitType].totalFruit = dataFruit[fruit.fruitType].totalFruit + fruit.stock
        })
        
        let totalBasket = Object.keys(dataFruit).length

        for (let [key, value] of Object.entries(dataFruit)) {
            console.log(`Buah pada wadah ${key} yaitu ${value.fruit}`)
            console.log(`Stok buah pada wadah ${key} berjumlah ${value.totalFruit}`)
        }
    
        let data = {
            totalBasket: totalBasket,
            basket: dataFruit
        }
        
        res.status(200).json(data)
    } catch (e) {
        console.log(e.message)
    }
}