import Fruit from "../../models/Case 1/FruitModel.js"

// Number 1
export const getFruitByName = async (req, res) => {
    try {
        const fruits = await Fruit.find()
        const dataName = new Set();
        fruits.forEach((fruit) => {
            dataName.add(fruit.fruitName.toUpperCase())
        })
        const dataArray = Array.from(dataName)
        console.log("Buah yang dimiliki Andi:")
        console.log(dataArray)
        res.status(200).json(dataArray)
    } catch (e) {
        console.log(e.message)
    }
}

// Number 2
export const getFruitBasket = async (req, res) => {
    try {
        const fruits = await Fruit.find()
        let dataFruit = {}
        fruits.forEach((fruit) => {
            if (dataFruit[fruit.fruitType] == undefined) {
                dataFruit[fruit.fruitType] = new Set()
            }
            dataFruit[fruit.fruitType] = dataFruit[fruit.fruitType].add(fruit.fruitName.toUpperCase())
        })
        
        let totalBasket = Object.keys(dataFruit).length
        console.log(`Jumlah Wadah Yang Dibutuhkan adalah ${totalBasket}`)

        for (let [key, value] of Object.entries(dataFruit)) {
            dataFruit[key] = Array.from(value)
            console.log(`Buah pada wadah ${key} yaitu ${dataFruit[key]}`)
        }
    
        let data = {
            totalBasket: totalBasket,
            basket: dataFruit
        }
        
        res.status(200).json(data)
    } catch (e) {
        console.log(e.message)
    }
}

// Number 3
export const getCountFruitBasket = async (req, res) => {
    try {
        const fruits = await Fruit.find()
        let dataFruit = {}
        fruits.forEach((fruit) => {
            if (dataFruit[fruit.fruitType] == undefined) {
                dataFruit[fruit.fruitType] = 0
            }
            dataFruit[fruit.fruitType] = dataFruit[fruit.fruitType] + fruit.stock
        })

        for (let [key, value] of Object.entries(dataFruit)) {
            console.log(`Stok buah pada wadah ${key} berjumlah ${value}`)
        }

        res.status(200).json(dataFruit)
    } catch (e) {
        console.log(e.message)
    }
}

// Number 2 and 3
export const getFruitBasketAndCount = async (req, res) => {
    try {
        const fruits = await Fruit.find()
        let dataFruit = {}
        fruits.forEach((fruit) => {
            if (dataFruit[fruit.fruitType] == undefined) {
                dataFruit[fruit.fruitType] = {}
                dataFruit[fruit.fruitType].fruit = new Set()
                dataFruit[fruit.fruitType].totalFruit = 0
            }
            dataFruit[fruit.fruitType].fruit  = dataFruit[fruit.fruitType].fruit.add(fruit.fruitName.toUpperCase())
            dataFruit[fruit.fruitType].totalFruit = dataFruit[fruit.fruitType].totalFruit + fruit.stock
        })
        
        let totalBasket = Object.keys(dataFruit).length
        for (let [key, value] of Object.entries(dataFruit)) {
            value.fruit = Array.from(value.fruit)
            dataFruit[key] = value
            console.log(`Buah pada wadah ${key} yaitu ${value.fruit}`)
            console.log(`Stok buah pada wadah ${key} berjumlah ${value.totalFruit}`)
        }
    
        let data = {
            totalBasket: totalBasket,
            basket: dataFruit
        }
        
        res.status(200).json(data)
    } catch (e) {
        console.log(e.message)
    }
}

export const createFruit = async (req, res) => {
    try {
        await new Fruit({
            fruidId: req.body.fruidId,
            fruitName: req.body.fruitName,
            fruitType: req.body.fruitType,
            stock: req.body.stock,
        }).save()
        res.status(201).json({ msg: "Fruit Created" })
    } catch (e) {
        console.log(e.message)
    }
}