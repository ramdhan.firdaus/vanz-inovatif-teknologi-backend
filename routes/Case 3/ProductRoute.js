import express from 'express'
import { getProduct, addProduct } from '../../controllers/Case 3/ProductController.js';

const router = express.Router()

router.get('/', getProduct)
router.post('/', addProduct)

export default router;