import express from 'express'
import { getStore, addStore } from '../../controllers/Case 3/StoreController.js';

const router = express.Router()

router.get('/', getStore)
router.post('/', addStore)

export default router;