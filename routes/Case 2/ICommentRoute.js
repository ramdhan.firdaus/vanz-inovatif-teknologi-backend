import express from 'express'
import { getTotalIComment, createIComment } from '../../controllers/Case 2/ICommentController.js';

const router = express.Router()

router.get('/', getTotalIComment)
router.post('/', createIComment)

export default router;