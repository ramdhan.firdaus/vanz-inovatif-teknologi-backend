import express from 'express'
import { getFruitByName, createFruit, getFruitBasket, getCountFruitBasket, getFruitBasketAndCount } from '../../controllers/Case 1/FruitController.js';

const router = express.Router()

router.get('/', getFruitByName)
router.get('/basket', getFruitBasket)
router.get('/count', getCountFruitBasket)
router.get('/basket-count', getFruitBasketAndCount)
router.post('/', createFruit)

export default router;