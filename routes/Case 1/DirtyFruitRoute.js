import express from 'express'
import { getCountFruitBasket, getFruitBasket, getFruitBasketAndCount, getFruitByName } from '../../controllers/Case 1/DirtyFruitController.js'

const router = express.Router()

router.get('/', getFruitByName)
router.get('/basket', getFruitBasket)
router.get('/count', getCountFruitBasket)
router.get('/basket-count', getFruitBasketAndCount)

export default router;