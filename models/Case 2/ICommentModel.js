import mongoose from 'mongoose'

const IComment = mongoose.model('iComment', {
    commentId: {
        type: Number,
        required: true,
    },
    commentContent: {
        type: String,
        required: true,
    },
    replies: {
        type: [Object]
    }
});

export default IComment