import mongoose from 'mongoose'

const Fruit = mongoose.model('fruit', {
    fruidId: {
        type: Number,
        required: true,
    },
    fruitName: {
        type: String,
        required: true,
    },
    fruitType: {
        type: String,
        required: true
    },
    stock: {
        type: Number,
        required: true
    }
});

export default Fruit