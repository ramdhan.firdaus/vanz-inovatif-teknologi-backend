import mongoose from 'mongoose'

const Store = mongoose.model('store', {
    storeId: {
        type: Number,
        required: true,
    },
    nameStore: {
        type: String,
        required: true,
    }
});

export default Store