import mongoose from 'mongoose'

const Product = mongoose.model('product', {
    productId: {
        type: Number,
        required: true,
    },
    nameProduct: {
        type: String,
        required: true,
    },
    store: {
        type: String,
        required: true,
    }
});

export default Product