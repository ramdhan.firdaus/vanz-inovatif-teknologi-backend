import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import './config/Database.js'

import fruitRoute from './routes/Case 1/FruitRoute.js'
import dirtyFruitRoute from './routes/Case 1/DirtyFruitRoute.js'

import iCommentRoute from './routes/Case 2/ICommentRoute.js'

import productRoute from './routes/Case 3/ProductRoute.js'
import storeRoute from './routes/Case 3/StoreRoute.js'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

app.use('/fruit', fruitRoute)
app.use('/dirty-fruit', dirtyFruitRoute)

app.use('/i-comment', iCommentRoute)

app.use('/product', productRoute)
app.use('/store', storeRoute)

const PORT = process.env.PORT || 8080

app.listen(PORT, () => {
    console.log(`Server is listening on ${PORT}`)
})